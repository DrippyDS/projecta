extends Control

onready var room_path = $"../../Room"
onready var anim_path = $"../../changer"

var current_side = 0
var next_side = 0
var sides = []

func _ready() -> void:
	sides = room_path.get_children()
	$left.connect("pressed", self, "set_side", [-1, true])
	$right.connect("pressed", self, "set_side", [1, true])
	change_side()


func change_side():
	sides[current_side].hide()
	sides[next_side].show()
	current_side = next_side


func set_side(idx : int, increment : bool = false):
	if increment:
		next_side = (current_side + idx) % sides.size()
	else:
		next_side = idx % sides.size()
	anim_path.play('change_scene')
