extends Control

var current_id = -1
var cells = []

func _ready() -> void:
	$Cell1.connect("pressed", self, "pressed_cell", [0])
	$Cell2.connect("pressed", self, "pressed_cell", [1])
	$Cell3.connect("pressed", self, "pressed_cell", [2])
	$Cell4.connect("pressed", self, "pressed_cell", [3])
	$Cell5.connect("pressed", self, "pressed_cell", [4])
	
	cells.append([$Cell1.rect_position + Vector2(16,16), null])
	cells.append([$Cell2.rect_position + Vector2(16,16), null])
	cells.append([$Cell3.rect_position + Vector2(16,16), null])
	cells.append([$Cell4.rect_position + Vector2(16,16), null])
	cells.append([$Cell5.rect_position + Vector2(16,16), null])
	
	pressed_cell(-1)


func pressed_cell(id : int) -> void:
	if current_id == id:
		current_id = -1
		$Select.hide()
	else:
		current_id = id
		$Select.show()
		$Select.rect_position = cells[id][0]

